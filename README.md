# Prosty formularz PHP
Bardzo proste zadanie "rozgrzewkowe" z języka PHP: napisanie prostego formularza
 z wykorzystaniem jQuery i Ajax-a.
 
## Krótka instrukcja uruchomienia
Aby uruchomić projekt, należy umieścić go na serwerze PHP (najwygodniej jest użyć pakietu XAMPP), oraz utworzyć bazę danych MySQL i zaimportować plik _baza.sql_.
Do poprawnego połączenia z bazą danych, trzeba poprawnie skonfigurować plik _connect.php_.