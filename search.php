<?php

require_once "User.php";

$user = new User();

$id = htmlspecialchars($_POST["id"]);
if (!is_numeric($id)) {
    $myObj = array("id" => "-1", "name" => "Złe dane wejściowe");
    echo json_encode($myObj);
} else {

    $result = $user->search($id);

    if ($result == null) {
        $myObj = array("id" => "0", "name" => "Nie znaleziono żadnego użytkownika");
        echo json_encode($myObj);
    } else {
        $obj = array("id" => $result['id'], "name" => html_entity_decode($result['name']), "last_name" => html_entity_decode($result['last_name']),
            "address" => html_entity_decode($result['address']));
        echo json_encode($obj);
    }
}
