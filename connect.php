<?php
Class Database
{
    public function connect()
    {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "staz";
        $port = 3306;

        try {
            $conn = new PDO("mysql:host=$servername; dbname=$dbname; charset=utf8", $username, $password);
            return $conn;

        } catch
        (PDOException $e) {
            echo "Błąd połączenia z bazą danych";
        }
    }
}