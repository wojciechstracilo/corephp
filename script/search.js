function search() {
    $.ajax({
        type: "POST",
        url: "search.php",
        dataType: 'json',
        data: $("#search").serialize(),
        success: function (response) {
            $("#result").show();
        }
    }).done(function (response) {
        if (response.id < 1) {
            document.getElementById("content").innerHTML = response.name;
            $('#searchTable').hide();

        } else {
            document.getElementById("content").innerHTML = "";

            $('#searchTable').show();
            $('#searchTable').find('tr:gt(0)').remove();
            $("#searchTable").append('<tr id="' + response.id + '"><td>' + response.id + '</td><td>' + response.name + '</td><td>'
                + response.last_name + '</td><td>' + response.address + '</td></tr>');
        }

    }).fail(function () {
        alert("Błąd");
    });
}

