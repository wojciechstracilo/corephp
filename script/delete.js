$(document).ready(function(){

    $(document).on('click', '.delete', function() {
        let tr = $(this).closest('tr');
        let del_id = tr.attr('id');

        $.ajax({
            url: 'delete.php',
            type: 'POST',
            data: { id:del_id},
            success: function(response){
                tr.fadeOut(600, function(){
                    tr.remove();
                });

            }
        });

    });

});