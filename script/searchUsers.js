function searchUsers() {

    $.ajax({
        type: "POST",
        url: "searchUsers.php",
        dataType: 'json',
        data: $("#search").serialize(),
        success: function (response) {
            $("#searchTable").show();
            $('#searchTable').find('tr:gt(0)').remove();

            $.each(response, function (index, value) {

                if (value['id'] < 1) {
                    $("#result").show();
                    document.getElementById("content").innerHTML = value['name'];
                    $('#searchTable').hide();
                } else {
                    $("#result").hide();

                    let id = value['id'];
                    let name = value['name'];
                    let last_name = value['last_name'];
                    let address = value['address'];

                    $("#searchTable").append('<tr id="' + id + '"><td>' + id + '</td><td>' + name + '</td><td>' + last_name + '</td><td>' + address + '</td></tr>');
                }

            });

        }

    }).fail(function () {
        alert("Błąd");
    });

}