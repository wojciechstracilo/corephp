function update(){
    $.ajax({
        type: "POST",
        url: "update.php",
        dataType: 'json',
        data: $("#addOrUpdate").serialize(),
        success: function (response) {

            $updateRow = ('<td>' + response.id + '</td><td>' + response.name + '</td><td>' + response.last_name + '</td><td>' + response.address + '</td>'
                + '<td><button type="button" class="delete">Usuń</button></td>'
                + '<td><button type="button" class="update">Edytuj</button></td>');

            $row = $("#table").find("tr#"+response.id).html($updateRow);
            // $row = $("#table").find("tr#"+response.id +"  td[name='']").html($updateRow);


            document.getElementById("title").innerHTML = "Dodaj użytkownika";
            $('#btn-add-update').attr( 'value', 'Dodaj');
            $('#addOrUpdate').attr( 'onsubmit', 'create()');

            $('#id, #name, #lastname, #address').val("");

        }
    }).fail(function () {
        alert("Błąd");
    });
}