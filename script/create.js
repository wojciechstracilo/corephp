function create() {
    $.ajax({
        type: "POST",
        url: "create.php",
        dataType: 'json',
        data: {
            name: $('#name').val(),
            lastname: $('#lastname').val(),
            address: $('#address').val()
        },
        success: function (data) {

            $("#table").append('<tr id="' + data.id + '"><td>' + data.id + '</td><td>' + data.name + '</td><td>' + data.last_name + '</td><td>' + data.address + '</td>'
                + '<td><button type="button" class="delete">Usuń</button></td>'
                + '<td><button type="button" class="update">Edytuj</button></td></tr>');


            $('#name, #lastname, #address').val("");

        }

    })
}