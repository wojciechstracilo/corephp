$(document).ready(function(){

    $(document).on('click', '.update', function() {
        let tr = $(this).closest('tr');
        let update_id = tr.attr('id');
        $.ajax({
            url: 'search.php',
            type: 'POST',
            dataType: 'json',
            data: { id:update_id},
            success: function(response){
                document.getElementById("title").innerHTML = "Edytuj użytkownika";
                $('#btn-add-update').attr('value', 'Zapisz');
                $('#addOrUpdate').attr('onsubmit', 'update()');

                $('#id').val(response.id);
                $('#name').val(response.name);
                $('#lastname').val(response.last_name);
                $('#address').val(response.address);

            }
        });

    });

});