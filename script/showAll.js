$(document).ready(function () {
    $.ajax({
        type: "GET",
        url: "showAll.php",
        dataType: 'json',

        success: function (data) {

            $.each(data, function (index, value) {
                let id = value['id'];
                let name = value['name'];
                let last_name = value['last_name'];
                let address = value['address'];

                $("#table").append('<tr id="' + id + '"><td>' + id + '</td><td>' + name + '</td><td>' + last_name + '</td><td>' + address + '</td>'
                    + '<td><button type="button" class="delete">Usuń</button></td>'
                    + '<td><button type="button" class="update">Edytuj</button></td></tr>');
            });

        }
    }).fail(function () {
        alert("Błąd");
    })
});
