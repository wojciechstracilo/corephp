<?php

require_once "User.php";

$column = $_POST["radiobutton"];
$value = $_POST["value"];

if ($column == "id") {
    if (!is_numeric($value)) {
        $json = [];
        array_push($json, ['id' => -1, 'name' => "Złe dane wejściowe", 'last_name' => 0, 'address' => 0]);
        echo json_encode($json);
    } else {
        $results = User::searchById($value);

        if (json_decode($results) == null){
            $json = [];
            array_push($json, ['id' => 0, 'name' => "Nie znaleziono żadnego użytkownika", 'last_name' => 0, 'address' => 0]);
            echo json_encode($json);
        } else {
            echo $results;
        }

    }

} elseif ($column == "name") {
    $results = User::searchByName($value);

    if (json_decode($results) == null){
        $json = [];
        array_push($json, ['id' => 0, 'name' => "Nie znaleziono żadnego użytkownika", 'last_name' => 0, 'address' => 0]);
        echo json_encode($json);
    } else {
        echo $results;
    }

} elseif ($column == "last_name") {
    $results = User::searchByLastName($value);

    if (json_decode($results) == null){
        $json = [];
        array_push($json, ['id' => 0, 'name' => "Nie znaleziono żadnego użytkownika", 'last_name' => 0, 'address' => 0]);
        echo json_encode($json);
    } else {
        echo $results;
    }

} elseif ($column == "address") {
    $results = User::searchByAddress($value);

    if (json_decode($results) == null){
        $json = [];
        array_push($json, ['id' => 0, 'name' => "Nie znaleziono żadnego użytkownika", 'last_name' => 0, 'address' => 0]);
        echo json_encode($json);
    } else {
        echo $results;
    }

}