<?php

require_once "connect.php";

class User
{

    public static function all()
    {
        try {
            $dat = new Database();
            $conn = $dat->connect();
            $sql = "SELECT * FROM users";
            $results = $conn->query($sql);

            $json = [];
            foreach ($results as $result) {
                array_push($json, ['id' => $result['id'], 'name' => $result['name'], 'last_name' => $result['last_name'], 'address' => $result['address']]);
            }

            return json_encode($json);

        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    public function search($id)
    {
        try {
            $dat = new Database();
            $conn = $dat->connect();
            $sql = "SELECT * FROM `users` WHERE `id` = ?";
            $query = $conn->prepare($sql);
            $query->bindParam(1, $id);
            $query->execute();
            $result = $query->fetch(PDO::FETCH_ASSOC);

            return $result;
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    public function searchById($id){
        try {
            $dat = new Database();
            $conn = $dat->connect();
            $sql = "SELECT * FROM `users` WHERE `id` = ?";
            $query = $conn->prepare($sql);
            $query->execute(array($id));

            $json = [];
            foreach ($query as $result) {
                array_push($json, ['id' => $result['id'], 'name' => $result['name'], 'last_name' => $result['last_name'], 'address' => $result['address']]);
            }

            return json_encode($json);
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    public function searchByName($name){
        try {
            $dat = new Database();
            $conn = $dat->connect();
            $sql = "SELECT * FROM `users` WHERE `name` LIKE ?";
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = $conn->prepare($sql);
            $query->execute(array("%$name%"));

            $json = [];
            foreach ($query as $result) {
                array_push($json, ['id' => $result['id'], 'name' => $result['name'], 'last_name' => $result['last_name'], 'address' => $result['address']]);
            }

            return json_encode($json);
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    public function searchByLastName($last_name){
        try {
            $dat = new Database();
            $conn = $dat->connect();
            $sql = "SELECT * FROM `users` WHERE `last_name` LIKE ?";
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = $conn->prepare($sql);
            $query->execute(array("%$last_name%"));

            $json = [];
            foreach ($query as $result) {
                array_push($json, ['id' => $result['id'], 'name' => $result['name'], 'last_name' => $result['last_name'], 'address' => $result['address']]);
            }

            return json_encode($json);
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    public function searchByAddress($address){
        try {
            $dat = new Database();
            $conn = $dat->connect();
            $sql = "SELECT * FROM `users` WHERE `address` LIKE ?";
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = $conn->prepare($sql);
            $query->execute(array("%$address%"));

            $json = [];
            foreach ($query as $result) {
                array_push($json, ['id' => $result['id'], 'name' => $result['name'], 'last_name' => $result['last_name'], 'address' => $result['address']]);
            }

            return json_encode($json);
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    public function create(string $name, string $lastname, string $address)
    {
        $dat = new Database();
        $conn = $dat->connect();
        $sql = "insert into users (name, last_name, address) values (?, ?, ?)";
        $query = $conn->prepare($sql);
        $query->execute(array($name, $lastname, $address));

        $id = $conn->lastInsertId();

        return $id;

    }

    public function delete($id){
        try {
            $dat = new Database();
            $conn = $dat->connect();
            $sql = "DELETE FROM `users` WHERE `id` = ?";
            $query = $conn->prepare($sql);
            $query->bindParam(1, $id);
            $query->execute();

            echo "Użytkownik został usunięty";
        } catch (PDOException $e) {
            echo "Błąd podczas wykonywania operacji";
        }
    }

    public function update($id, $name, $lastname, $address){
        try {
            $dat = new Database();
            $conn = $dat->connect();
            $sql = "UPDATE `users` SET name=?, last_name=?, address=? where id=?";
            $query = $conn->prepare($sql);
            $query->execute(array($name, $lastname, $address, $id));

            return $id;
        } catch (PDOException $e) {
            echo "Błąd podczas wykonywania operacji";
        }
    }

}