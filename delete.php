<?php

require_once "User.php";

try {

    $id = htmlspecialchars($_POST["id"]);
    if (!is_numeric($id)) {
        $myObj = array("id" => "-1", "name" => "Złe dane wejściowe");
        echo json_encode($myObj);
    } else {

        $result = User::search($id);

        if ($result == null) {
            $myObj = array("id" => "0", "name" => "Nie ma użytkownika o takim identyfikatorze");
            echo json_encode($myObj);
        } else {
            $results = User::delete($_POST["id"]);
            return "Usunięto użytkownika";
        }
    }

} catch (PDOException $e) {
    return "Błąd połączenia z bazą danych";
}