<?php

require_once "User.php";

if (($_POST["name"] == null | strlen($_POST["name"]) < 3 | strlen($_POST["name"]) > 50)
    | ($_POST["lastname"] == null | strlen($_POST["lastname"]) < 3 | strlen($_POST["lastname"]) > 50)
    | ($_POST["address"] == null | strlen($_POST["address"]) < 5 | strlen($_POST["address"]) > 80)) {

    echo "Złe dane wejściowe";

} else {
    try {
        $name = $_POST["name"];
        $lastname = $_POST["lastname"];
        $address = $_POST["address"];

        $newUserId = User::create(htmlentities($name), htmlentities($lastname), htmlentities($address));
        $result = User::search($newUserId);
        $obj = array("id" => $result['id'], "name" => $result['name'], "last_name" => $result['last_name'], "address" => $result['address']);
        echo json_encode($obj);

    } catch (PDOException $e) {
        return "Błąd połączenia z bazą danych";
    }
}