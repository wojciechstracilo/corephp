<?php

require_once "User.php";


$id = htmlspecialchars($_POST["id"]);
if (!is_numeric($id)) {
    $myObj = array("id" => "-1", "name" => "Złe dane wejściowe");
    echo json_encode($myObj);
} else {

    $result = User::search($id);

    if ($result == null) {
        $myObj = array("id" => "0", "name" => "Nie ma użytkownika o takim identyfikatorze");
        echo json_encode($myObj);
    } else {
        $id = User::update($_POST["id"],$_POST["name"],$_POST["lastname"],$_POST["address"]);

        $result = User::search($id);

        $obj = array("id" => $result['id'], "name" => htmlentities($result['name']), "last_name" => htmlentities($result['last_name']),
            "address" => htmlentities($result['address']));

        echo json_encode($obj);
    }
}

